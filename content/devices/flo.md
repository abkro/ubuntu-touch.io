---
codename: 'flo'
name: 'Nexus 7 2013 WiFi'
comment: 'legacy'
deviceType: 'tablet'
image: 'http://cloud.addictivetips.com/wp-content/uploads/2013/02/Ubuntu-Touch-Nexus-10-lockscreen.jpg'
maturity: .8
---

## The budget tablet

The Nexus 7 2013 WiFi is a great tablet to use Ubuntu Touch. It's often possible to find second-hand devices at very reasonable prices.

**Note**: The [Nexus 7 2013 LTE (deb)](/device/deb) is supported as well, but other Devices from the Nexus 7 line might not work.
