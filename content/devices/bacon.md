---
codename: 'bacon'
name: 'Oneplus One'
comment: ''
deviceType: 'phone'
image: 'https://ubports.com/web/image/1452/OneplusOne.jpeg'
maturity: 1
---

## Elegant and powerful

You wish to be independent from Google and Apple, but don't want to quit on awesome modern hardware? You want to re-claim control over your smartphone and still enjoy a slick user experience? It's time for you to install Ubuntu Touch on your OnePlus One!

Porting Ubuntu Touch to the Oneplus One was the first collaborative project for the UBports Community in 2015, and is one of the most popular Ubuntu Touch devices to this day. The device was chosen because of its open software stack and active development community.

The Oneplus One is among the most powerful UBports devices, and with the free and open-source mobile operating system Ubuntu Touch, it is a great alternative to Cyanogen Mod, LineageOS and Android.
