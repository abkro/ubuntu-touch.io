---
codename: 'pinephone'
name: 'Pinephone'
comment: 'experimental'
deviceType: 'phone'
image: 'https://store.pine64.org/wp-content/uploads/2020/03/CommunityEditionUBports.png'
video: 'https://www.youtube.com/embed/3Ne6G0-hn9g'
noInstall: true
maturity: .75
---

The [Pinephone](https://www.pine64.org/pinephone/) is an affordable ARM-based linux phone.

You can find [installation instructions on GitLab](https://gitlab.com/ubports/community-ports/pinephone).

### Device specifications
|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Allwinner A64                                                         |
|          CPU | 4x 1152 MHz Cortex-A53                                                |
| Architecture | AArch64                                                               |
|          GPU | Mali-400 MP2                                                          |
|      Display | 5.95" 720x1440 IPS                                                    |
|      Storage | 16 GB / 32 GB                                                         |
|       Memory | 2 GB / 3 GB                                                           |
|      Cameras | Single 5MP, 1/4″, LED Flash<br>Single 2MP, f/2.8, 1/5″                |
|   Dimensions | 160.5mm x 76.6mm x 9.2mm                                              |

### Discussions
[Pine64 subforum](https://forum.pine64.org/forumdisplay.php?fid=125)

[UBports subforum](https://forums.ubports.com/topic/2403/pinephone)

[Pinephone subreddit](https://www.reddit.com/r/pinephone/)

[IRC](https://www.pine64.org/web-irc/) (`#pinephone` on irc.pine64.org)

`#pinephone` on [Pine64 Discord](https://discord.com/invite/DgB7kzr)

`#pinephone:matrix.org`

[Telegram chat](https://t.me/pinephone)

### Source repository
https://gitlab.com/ubports/community-ports/pinephone

### Builds
https://ci.ubports.com/job/rootfs/job/rootfs-pinephone/
