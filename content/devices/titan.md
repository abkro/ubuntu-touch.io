---
codename: 'titan'
name: 'Moto G (2014)'
comment: 'community device'
deviceType: 'phone'
noInstall: true
maturity: .1
---

This device is still experimental and has to be [installed manually](https://forum.xda-developers.com/moto-g-2014/development/experimental-ubuntu-touch-titan-t3608846).