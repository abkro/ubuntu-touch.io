---
codename: 'turbo'
name: 'Meizu Pro 5'
comment: 'legacy'
deviceType: 'phone'
image: 'https://topcom.lt/wp-content/uploads/2016/02/Meizu-Pro-5-Ubuntu-Edition.jpg'
maturity: .9
---

## Pursuit of the ultimate perfection

On the top-end of the price-range of the previously available Ubuntu Touch devices, the Meizu Pro 5 combines an elegant operating system with the hardware it deserves to run on.

**Note**: Meizu Pro 5 devices that are sold with Android have a locked bootloader, so installing UBports' version of Ubuntu Touch is only possible on devices that come with Ubuntu in the first place.
