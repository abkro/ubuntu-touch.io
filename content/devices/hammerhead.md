---
codename: 'hammerhead'
name: 'Nexus 5'
deviceType: 'phone'
portType: 'Legacy'
maturity: .95
portStatus:
  -
    categoryName: 'Actors'
    features:
      -
        id: 'manualBrightness'
        value: '+'
        bugtracker: '#'
      -
        id: 'notificationLed'
        value: '+'
        bugtracker: '#'
      -
        id: 'torchlight'
        value: '+'
        bugtracker: '#'
      -
        id: 'vibration'
        value: '+'
        bugtracker: '#'
  -
    categoryName: 'Camera'
    features:
      -
        id: 'flashlight'
        value: '+'
        bugtracker: '#'
      -
        id: 'photo'
        value: '+'
        bugtracker: '#'
      -
        id: 'video'
        value: '-'
        bugtracker: 'https://gitlab.com/capsia'
      -
        id: 'switchCamera'
        value: '+'
        bugtracker: '#'
  -
    categoryName: 'Cellular'
    features:
      -
        id: 'carrierInfo'
        value: '+'
        bugtracker: '#'
      -
        id: 'dataConnection'
        value: '+'
        bugtracker: '#'
      -
        id: 'calls'
        value: '+'
        bugtracker: '#'
      -
        id: 'mms'
        value: '+'
        bugtracker: '#'
      -
        id: 'pinUnlock'
        value: '+'
        bugtracker: '#'
      -
        id: 'sms'
        value: '+'
        bugtracker: '#'
      -
        id: 'audioRoutings'
        value: '+'
        bugtracker: '#'
      -
        id: 'voiceCall'
        value: '+'
        bugtracker: '#'
  -
    categoryName: 'Endurance'
    features:
      -
        id: 'batteryLifetimeTest'
        value: '+'
        bugtracker: '#'
      -
        id: 'noRebootTest'
        value: '+'
        bugtracker: '#'
  -
    categoryName: 'GPU'
    features:
      -
        id: 'uiBoot'
        value: '+'
        bugtracker: '#'
      -
        id: 'videoAcceleration'
        value: '-'
        bugtracker: '#'
  -
    categoryName: 'Misc'
    features:
      -
        id: 'anboxPatches'
        value: '-'
        bugtracker: '#'
      -
        id: 'apparmorPatches'
        value: '+'
        bugtracker: '#'
      -
        id: 'batteryPercentage'
        value: '+'
        bugtracker: '#'
      -
        id: 'offlineCharging'
        value: '+'
        bugtracker: '#'
      -
        id: 'onlineCharging'
        value: '+'
        bugtracker: '#'
      -
        id: 'recoveryImage'
        value: '+'
        bugtracker: '#'
      -
        id: 'factoryReset'
        value: '+'
        bugtracker: '#'
      -
        id: 'rtcTime'
        value: '+'
        bugtracker: '#'
      -
        id: 'shutdown'
        value: '+'
        bugtracker: '#'
      -
        id: 'wirelessCharging'
        value: '+'
        bugtracker: '#'
  -
    categoryName: 'Network'
    features:
      -
        id: 'bluetooth'
        value: '+'
        bugtracker: '#'
      -
        id: 'flightMode'
        value: '+'
        bugtracker: '#'
      -
        id: 'hotspot'
        value: '+'
        bugtracker: '#'
      -
        id: 'nfc'
        value: '-'
        bugtracker: '#'
      -
        id: 'wifi'
        value: '+'
        bugtracker: '#'
  -
    categoryName: 'Sensors'
    features:
      -
        id: 'autoBrightness'
        value: '+'
        bugtracker: '#'
      -
        id: 'gps'
        value: '+'
        bugtracker: '#'
      -
        id: 'proximity'
        value: '+'
        bugtracker: '#'
      -
        id: 'rotation'
        value: '+'
        bugtracker: '#'
      -
        id: 'touchscreen'
        value: '+'
        bugtracker: '#'
  -
    categoryName: 'Sound'
    features:
      -
        id: 'earphones'
        value: '+'
        bugtracker: '#'
      -
        id: 'loudspeaker'
        value: '+'
        bugtracker: '#'
      -
        id: 'microphone'
        value: '+'
        bugtracker: '#'
      -
        id: 'volumeControl'
        value: '+'
        bugtracker: '#'
  -
    categoryName: 'USB'
    features:
      -
        id: 'mtp'
        value: '+'
        bugtracker: '#'
      -
        id: 'adb'
        value: '+'
        bugtracker: '#'
      -
        id: 'externalMonitor'
        value: '+'
        bugtracker: '#'
deviceInfo:
  -
    id: 'cpu'
    value: 'Krait 400, 2300MHz, 4 Cores'
  -
    id: 'chipset'
    value: 'Qualcomm Snapdragon 800, MSM8974AA'
  -
    id: 'gpu'
    value: 'Qualcomm Adreno 330, 450MHz, 4 Cores'
  -
    id: 'rom'
    value: '16/32GB'
  -
    id: 'ram'
    value: '2GB, 800MHz'
  -
    id: 'android'
    value: '4.4.3'
  -
    id: 'battery'
    value: '2300 mAh'
  -
    id: 'display'
    value: '1080x1920 pixels, 5 in'
  -
    id: 'rearCamera'
    value: '8MP'
  -
    id: 'frontCamera'
    value: '1.2MP'
contributors:
  -
    name: 'NeoTheThird'
    photo: 'https://forums.ubports.com/assets/uploads/profile/229-profileavatar.png'
    forum: 'https://forums.ubports.com/user/neothethird'
  -
    name: 'AppLee'
    photo: 'https://forums.ubports.com/assets/uploads/profile/2944-profileavatar.png'
    forum: 'https://forums.ubports.com/user/applee'
  -
    name: 'Keneda'
    photo: 'https://forums.ubports.com/assets/uploads/profile/3285-profileavatar.png'
    forum: 'https://forums.ubports.com/user/keneda'
externalLinks:
  -
    name: 'Telegram - Nexus 5'
    link: 'https://t.me/joinchat/Bd_29FDzIzx7p7tlKYVFFw'
    icon: 'telegram'
  -
    name: 'Forum Post'
    link: 'https://forums.ubports.com/topic/3777/call-for-testing-nexus-5-hammerhead-owners/32'
    icon: 'yumi'
  -
    name: 'Repository'
    link: 'https://github.com/masneyb/nexus-5-upstream'
    icon: 'github'
installHelp:
  default: true
  rebootToBootloader: 'Press and hold power and volume down buttons '
---

## The Comfort of Brilliant Convergence

Are you tired of being dependent on 'The Big Two' and their services and apps? Do you want to regain control over your personal and private data on your smartphone? Install Ubuntu Touch on your Nexus 5! It keeps your Nexus 5 secure, because everything unsafe is blocked by default and the OS is virtually free of viruses and other malware that can extract your private data.

[More about convergence...](https://ubports.com/devices/nexus5-convergence).

